var hbs = require('handlebars');
var path = require('path');
var fs = require('fs');

var simpleHbs = function(){
    this.options = {};
    this.set = function(name, action){
        if(name == "views"){
            this.options.views = action;
        }
    }
    this.Render = function(view, data){
        if(view.indexOf(".") == -1){
            view += ".hbs";
        }
        var myPath = "";
        if(this.options.views){
            myPath = path.join(this.options.views, view)
        }
        else{
            myPath = view;
        }
        var file = fs.readFileSync( myPath ,'utf8');
        var template = hbs.compile(file);
        return template(data);
    }
    this.endsWith = function(str, end){
        var strEnd = "";
        for(i = (str.length - end.length); i < str.length; i++){
            strEnd += str[i];
        }
        return strEnd == end;
    }
}
module.exports = new simpleHbs();
